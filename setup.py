from setuptools import find_namespace_packages, find_packages, setup

setup(
    name="stella_cloud",
    version="0.1",
    description="A codeframwork to upload stellarator boundaries and coils to stellarators.cloud",
    url="https://gitlab.mpcdf.mpg.de/jtl/py_stella_cloud",
    author="Jorrit Lion",
    author_email="jorrit.lion@ipp.mpg.de",
    license="MIT",
    packages=find_packages() + find_namespace_packages(),
    install_requires=["numpy", "requests", "scipy", "pathlib"],
    python_requires=">=3.6.*",
    zip_safe=False,
    scripts=["scripts/stella_cloud_post_fixed.py", "scripts/stella_cloud_write_vmecinput.py"],
)
