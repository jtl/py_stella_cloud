"""
print_format.py
====================================
A library that formats to the screen print outs

.. moduleauthor:: Jorrit Lion <jorrit.lion@ipp.mpg.de>
"""


class bcolors:
    INFO = "\033[92m"  # GREEN
    WARNING = "\033[93m"  # YELLOW
    ERROR = "\033[91m"  # RED
    VAR = "\033[94m"  # BLUE
    RESET = "\033[0m"  # RESET COLOR


def print_line_star():
    print(
        "********************************************************************************************"
    )
    pass


def print_line_dash():
    print(
        "--------------------------------------------------------------------------------------------"
    )
    pass


def print_subsection(name=""):
    """
    Prints
    """
    print(
        "--------------------------------------------------------------------------------------------"
    )
    print("{:^90}".format(name))
    print()
    return 1


def print_section(name=""):
    """
    Prints
    """
    print_line_star()
    print("{:^90}".format(name))
    print()
    return 1


def end_subsection(name=""):
    """
    Prints
    """
    print()
    print("{:^90}".format(name))
    print_line_dash()
    print()
    return 1


def print_variable(name, value, unit=None):
    """
    Prints a variable with a unit if given.
    Format is 92 characters (line length)
    """
    if not unit:
        print(bcolors.VAR + "VAR: " + bcolors.RESET, "{:<50}  {:>40.4E}".format(name, value))
    else:
        print(
            bcolors.VAR + "VAR: " + bcolors.RESET,
            "{:<50}  {:>40.4E}".format(name + " [" + unit + "]", value),
        )
    return 1


def print_variable_2(name1, value1, name2, value2, unit=None):
    """
    Prints a variable with a unit if given AND takes an additional agument
    """
    if not unit:
        #            32         38      22 = 92 matches character line number
        print(
            bcolors.VAR + "VAR: " + bcolors.RESET,
            "{:<26}  {:>4}, {:<36}  {:>20.4E}".format(name1, value1, name2, value2),
        )
    else:
        print(
            bcolors.VAR + "VAR: " + bcolors.RESET,
            "{:<26}  {:>4}, {:<36}  {:>20.4E}".format(
                name1, value1, name2 + " [" + unit + "]", value2
            ),
        )
    return 1


def print_status(*status):
    """
    Prints a status that the computation is in
    """
    print(bcolors.INFO + "STATUS: " + bcolors.RESET, *status, "...")
    return 1


def print_info(*names):
    """
    Prints an information to the screen
    """
    print(bcolors.INFO + "INFO! " + bcolors.RESET, *names)
    return 1


def print_warning(*names):
    """
    Prints a warning to the screen
    """
    print(bcolors.WARNING + "WARNING! " + bcolors.RESET, *names)
    return 1


def print_error(*names):
    """
    Prints an error to the screen
    """
    print(bcolors.ERROR + "ERROR! " + bcolors.RESET, *names)
    return 1
