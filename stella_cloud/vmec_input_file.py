"""
vmec_prepare.py
====================================
The module that generates a vmec file based on the coil only.

.. moduleauthor:: Jorrit Lion <jorrit.lion@ipp.mpg.de>
"""
import json
import os
from pathlib import Path

import numpy as np

from stella_cloud.print_statements import print_error, print_warning


class _vmec_input_list_int:
    def __init__(self, name: str, array) -> None:
        self.name = name
        self.value = list(array)

    def output(self, outputfile):
        outputfile.write(
            self.name + " = {:}\n".format(", ".join(list(map(str, self.value))))
        )  # cumbersome way to convert list of ints to list of strings


class _vmec_input_list_float:
    def __init__(self, name: str, array) -> None:
        self.name = name
        self.value = list(array)

    def output(self, outputfile):
        outputfile.write(self.name + " = {:}\n".format(", ".join(list(map(str, self.value)))))


class _vmec_input_float:
    def __init__(self, name: str, value: float) -> None:
        self.name = name
        self.value = value

    def output(self, outputfile):
        outputfile.write("{:} = {:}\n".format(self.name, self.value))


class _vmec_input_int:
    def __init__(self, name: str, value: int) -> None:
        self.name = name
        self.value = value

    def output(self, outputfile):
        outputfile.write("{:} = {:}\n".format(self.name, self.value))


class _vmec_input_bool:
    def __init__(self, name: str, value: str) -> None:
        self.value = value
        self.name = name

    def output(self, outputfile):
        "Note that we dont have '' here!"
        outputfile.write("{:} = {:}\n".format(self.name, self.value))


class _vmec_input_string:
    def __init__(self, name: str, value: str) -> None:
        self.name = name
        self.value = value

    def output(self, outputfile):
        outputfile.write("{:} = '{:}'\n".format(self.name, self.value))


class _vmec_input_matrix:  # Used for rmnc and zmns
    def __init__(self, name: str, fourier_list: list) -> None:
        self.name = name
        self.xm = fourier_list[:, 1].astype(int)  # Hopefully I get numpy arrays here
        self.xn = fourier_list[:, 0].astype(int)
        self.zmns = fourier_list[:, 3]
        self.rmnc = fourier_list[:, 2]

    def output(self, outputfile):
        for xn, xm, rmnc, zmns in zip(self.xm, self.xn, self.rmnc, self.zmns):
            outputfile.write("RBC({:},{:}) = {:}  ".format(xn, xm, rmnc))
            outputfile.write("ZBS({:},{:}) = {:}\n".format(xn, xm, zmns))


class vmec_input_file:
    """
    represents the vmec input file
    """

    def __init__(self, **kwargs):
        """Initializes the vmec file from a given file

        Args:
            filename ([str]): Location of the vmec (free or fixed boundary)
        """

        script_dir = Path(os.path.dirname(__file__))  # local path
        config_filename = os.path.join(script_dir.parent, "vmec_defaults.json")
        defaults = json.load(open(config_filename))

        self.parameters = {}

        for key in defaults.keys():
            # Iterates through the keys as defined in "vmec_defaults.json" and checks if any of the kwargs is set for this.
            value = kwargs.pop(key, defaults[key][1])
            valuetype = defaults[key][0]
            if value is None:
                value = defaults[key][1]

            if valuetype == "float":
                obj = _vmec_input_float(key, value)
            elif valuetype == "int":
                obj = _vmec_input_int(key, value)
            elif valuetype == "string":
                obj = _vmec_input_string(key, value)
            elif valuetype == "list_int":
                obj = _vmec_input_list_int(key, value)
            elif valuetype == "list_float":
                obj = _vmec_input_list_float(key, value)
            elif valuetype == "matrix":
                obj = _vmec_input_matrix(key, value)
            elif valuetype == "bool":
                obj = _vmec_input_bool(key, value)
            else:
                print_error(
                    "Check defaults in vmec_defaults.json! I don't know: ", defaults[key][0]
                )

            self.parameters[key] = obj

        for kwarg in kwargs:
            print_warning("In vmec_input file. I got the key", kwarg, ", but I didn't use it!")

    @classmethod
    def from_stella_cloud_id(cls, id: str, set="fixed"):
        """
        Initializes the vmec input file object from an stella_client ID.

        Args:
            set: Either "fixed", "free" or "NearAxisQI"
        """
        from stella_cloud.APIClient import Stella_Client
        from stella_cloud.utilities import xm_xn_from_ntor_mpol

        if set == "NearAxisQI":
            print_warning(
                "You initialize from the NearAxisQI database. Note that NSS eq. can't be"
                " initialized yet."
            )

        client = Stella_Client()
        data = client.get_by_id(id, set=set)

        script_dir = Path(os.path.dirname(__file__))  # local path
        config_filename = os.path.join(script_dir.parent, "default_data.json")
        default_data = json.load(open(config_filename))
        for key in default_data.keys():
            if key not in data.keys():
                data[key] = default_data[key]

        # to generate the matrix for rbc and zbs first generate this
        xm, xn = xm_xn_from_ntor_mpol(data["ntor"], data["mpol"])
        RBC = np.array(data["Fourier_Coefficients"]["rmnc"])
        ZBS = np.array(data["Fourier_Coefficients"]["zmns"])

        # format is [[n,m,rbc,zbs],]
        fourier_list = np.transpose([xn, xm, RBC, ZBS])

        return cls(
            RBCZBS=fourier_list,
            RAXIS_CC=data["Axis"]["raxis_cc"],
            ZAXIS_CS=data["Axis"]["zaxis_cs"],
            PHIEDGE=data["phiedge"],
            NFP=data["nfp"],
            MPOL=data["mpol"],
            NTOR=data["ntor"],
            AM=data["Power_Series"]["AM"],
            AC=data["Power_Series"]["AC"],
            CURTOR=data["CURTOR"],
            NCURR=data["NCURR"],
            PRECON_TYPE=data["PRECON_TYPE"],
            PMASS_TYPE=data["PMASS_TYPE"],
            PIOTA_TYPE=data["PIOTA_TYPE"],
            PCURR_TYPE=data["PCURR_TYPE"],
            SPRES_PED=data["SPRES_PED"],
            PRES_SCALE=data["PRES_SCALE"],
        )

    @classmethod
    def from_stella_cloud_name(cls, name: str, set="fixed"):
        """
        Initializes the vmec input file object from a stella_client NAME (e.g. try "HSR-5" with set="fixed").
        """
        from stella_cloud.APIClient import Stella_Client

        client = Stella_Client()
        try:
            data = (client.get_by_name(name, set=set))[
                0
            ]  # this returns a list, so take the first item here
        except:
            print_error("No configuration found with the name ", name, " in ", set)

        from stella_cloud.utilities import xm_xn_from_ntor_mpol

        # to generate the matrix for rbc and zbs first generate this
        xm, xn = xm_xn_from_ntor_mpol(data["ntor"], data["mpol"])
        RBC = np.array(data["Fourier_Coefficients"]["rmnc"])
        ZBS = np.array(data["Fourier_Coefficients"]["zmns"])

        # format is [[n,m,rbc,zbs],]
        fourier_list = np.transpose([xn, xm, RBC, ZBS])

        return cls(
            RBCZBS=fourier_list,
            RAXIS_CC=data["Axis"]["raxis_cc"],
            ZAXIS_CS=data["Axis"]["zaxis_cs"],
            PHIEDGE=data["phiedge"],
            NFP=data["nfp"],
            MPOL=data["mpol"],
            NTOR=data["ntor"],
            AM=data["Power_Series"]["AM"],
            AC=data["Power_Series"]["AC"],
            CURTOR=data["CURTOR"],
            NCURR=data["NCURR"],
            PRECON_TYPE=data["PRECON_TYPE"],
            PMASS_TYPE=data["PMASS_TYPE"],
            PIOTA_TYPE=data["PIOTA_TYPE"],
            PCURR_TYPE=data["PCURR_TYPE"],
            SPRES_PED=data["SPRES_PED"],
            PRES_SCALE=data["PRES_SCALE"],
        )

    @classmethod
    def parse(cls, filelocation: str):
        """
        Loads the class from a vmec input file
        """
        rmnc, _, _ = cls.parse_rmnc(filelocation)
        zmns, xn, xm = cls.parse_zmns(filelocation)

        raxis = cls.parse_r_axis(filelocation)
        zaxis = cls.parse_z_axis(filelocation)

        parameters = cls.parse_parameters(filelocation)

        extcur = cls.parse_extcur(filelocation)

        try:
            nfp = int(parameters["NFP"])
        except BaseException:
            print_warning("No NFP found in VMEC file. Initilaizing with symmetry 1")
            nfp = 1

        mpol = max(xm)
        ntor = int(max(xn) / nfp)  # not super sure about these two

        return cls(
            rmnc=rmnc,
            zmns=zmns,
            raxis=raxis,
            zaxis=zaxis,
            parameters=parameters,
            NFP=nfp,
            EXTCUR=extcur,
            xn=xn,
            xm=xm,
            ntor=ntor,
            mpol=mpol,
        )

    @classmethod
    def from_netcdf_and_input(cls, inputfile: str, vmecnetcdf_path: str):
        """
        Loads the class from a vmec input file BUT uses the netcdf file for the boundary

        CARE: xn are NOT the vmec xns they are vmec xn/nfp
        """
        from scipy.io import netcdf_file

        try:
            wout_netcdf = netcdf_file(vmecnetcdf_path, "r")
        except:
            print_error("No such vmec netcdf path:", vmecnetcdf_path)
            return

        zmns = wout_netcdf.variables["zmns"][:][-1]
        rmnc = wout_netcdf.variables["rmnc"][:][-1]

        nfp = int(wout_netcdf.variables["nfp"][:])

        xn = np.array(wout_netcdf.variables["xn"][:] / nfp).astype(int)
        xm = np.array(wout_netcdf.variables["xm"][:]).astype(int)

        ntor = wout_netcdf.variables["ntor"][:]
        mpol = wout_netcdf.variables["mpol"][:]

        raxis = wout_netcdf.variables["raxis_cc"][:]
        zaxis = wout_netcdf.variables["zaxis_cs"][:]

        parameters = cls.parse_parameters(inputfile)

        extcur = cls.parse_extcur(inputfile)
        return cls(
            rmnc=rmnc,
            zmns=zmns,
            raxis=raxis,
            zaxis=zaxis,
            parameters=parameters,
            NFP=nfp,
            EXTCUR=extcur,
            xn=xn,
            xm=xm,
            ntor=ntor,
            mpol=mpol,
        )

    @staticmethod
    def parse_rmnc(filelocation: str):
        """
        Parses the vmec file and returns it as an (2*ntor+1,mpol) array
        """
        import re

        # Read in RBC
        raw_content = [
            re.findall(r"RBC\(([\s\-0-9]*),([0-9]*)\)[\s]*=[\s]*([-+0-9\.eE]*) ", line)
            for line in open(filelocation)
        ]
        raw_content = list(filter(None, raw_content))

        nlist = []
        mlist = []
        rmnc = []

        for item in raw_content:
            n = int(item[0][0])  # item is of format [('0', '0', '5.5586e+00')]
            m = int(item[0][1])
            number = float(item[0][2])
            nlist.append(n)
            mlist.append(m)
            rmnc.append(number)

        return rmnc, nlist, mlist

    @staticmethod
    def parse_zmns(filelocation: str):
        """
        Parses the vmec file and returns it as an (2*ntor+1,mpol) array
        """
        import re

        # Read in zmns

        raw_content = [
            re.findall(r"ZBS\(([\s\-0-9]*),([0-9]*)\)[\s]*=[\s]*([-+0-9\.eE]*)", line)
            for line in open(
                filelocation
            )  # missing space at the end is important (compared to rbc)
        ]
        raw_content = list(filter(None, raw_content))

        nlist = []
        mlist = []
        zmns = []

        for item in raw_content:
            n = int(item[0][0])  # item is of format [('0', '0', '5.5586e+00')]
            m = int(item[0][1])
            number = float(item[0][2])
            nlist.append(n)
            mlist.append(m)
            zmns.append(number)

        return zmns, nlist, mlist

    @staticmethod
    def parse_r_axis(filelocation: str):
        """
        Parses the vmec file and returns it as an (2*ntor+1,mpol) array
        """
        import re

        import numpy as np

        # Read in zmns

        raw_content = [
            re.findall(r"(?:RAXIS_CC|RAXIS)[\s]*=[\s]*([-+0-9\.eE\s]*)$", line)
            for line in open(
                filelocation
            )  # missing space at the end is important (compared to rbc)
        ]
        raw_content = list(filter(None, raw_content))[-1]  # take the last occurence
        raxis = np.array(raw_content[0].split()).astype(np.float64)

        return raxis

    @staticmethod
    def parse_z_axis(filelocation: str):
        """
        Parses the vmec file and returns it as an (2*ntor+1,mpol) array
        """
        import re

        import numpy as np

        # Read in zmns

        raw_content = [
            re.findall(r"(?:ZAXIS_CS|ZAXIS)[\s]*=[\s]*([-+0-9\.eE\s]*)$", line)
            for line in open(
                filelocation
            )  # missing space at the end is important (compared to rbc)
        ]
        raw_content = list(filter(None, raw_content))[-1]  # take the last occurence
        zaxis = np.array(raw_content[0].split()).astype(np.float64)

        return zaxis

    @staticmethod
    def parse_extcur(filelocation: str):
        """
        Parses the vmec file and returns it as an (2*ntor+1,mpol) array
        """
        import re

        import numpy as np

        # Read in zmns

        try:
            raw_content = [
                re.findall(r"EXTCUR[\s]*=[\s]*([-+0-9\.\,eE ]*)$", line)
                for line in open(
                    filelocation
                )  # missing space at the end is important (compared to rbc)
            ]
            raw_content = list(filter(None, raw_content))[-1]  # take the last occurence

            try:
                EXTCUR = np.array(raw_content[0].split(",")).astype(np.float64)
            except:  # tried to split on komma. If this does not work try to split on space
                EXTCUR = np.array(raw_content[0].split()).astype(np.float64)

        except:
            print_warning("No EXTCUR found. Fixed Boundary? No problemo, setting it to None.")
            EXTCUR = None  # [0, 0, 0, 0, 0]

        return EXTCUR

    @staticmethod
    def parse_parameters(filelocation: str):
        """
        Parses the vmec file and returns all set parameters as a dictionary
        """
        import re

        raw_content = [
            re.findall(r"([\w\d]+) *= *([-+0-9\.eE, TF]+)[\n\s]", open(filelocation).read())
        ]
        raw_content = list(filter(None, raw_content))[0]

        mydict = {}
        for item in raw_content:
            mydict[str(item[0])] = item[1]

        # Now add the things with a string
        # Like:  MGRID_FILE = 'mgrid_w7x_nv36_hires.nc'
        raw_content = [
            re.findall(r"([\w\d]+) *= *([\'\"][\w_\.]+[\'\"])[\n\s]", open(filelocation).read())
        ]
        raw_content = list(filter(None, raw_content))[0]

        for item in raw_content:
            mydict[str(item[0])] = item[1]

        # Delete Raxis and Zaxis
        if "RAXIS" in mydict.keys():
            mydict.pop("RAXIS")
        if "ZAXIS" in mydict.keys():
            mydict.pop("ZAXIS")
        if "ZAXIS_CS" in mydict.keys():
            mydict.pop("ZAXIS_CS")
        if "RAXIS" in mydict.keys():
            mydict.pop("RAXIS_CC")

        return mydict

    def write_input_file(self, filelocation):
        """
        Args:
            filelocation ([type]): [description]
        """

        # First write parameters
        with open(filelocation, "w") as out:
            out.write("&INDATA\n")

            # First write parameters
            for key in self.parameters.keys():
                self.parameters[key].output(out)

            out.write("/\n")

    def scale_R(self, scaling_parameter):
        """Scales the vmec file in R"""
        self.rmnc = np.multiply(
            self.rmnc, scaling_parameter
        )  # If not np.multiply, it throws an "can't multiply sequence by non-int of type 'numpy.float64'" error...
        self.zmns = np.multiply(self.zmns, scaling_parameter)
        self.raxis = np.multiply(self.raxis, scaling_parameter)
        self.zaxis = np.multiply(self.zaxis, scaling_parameter)

        self.parameters["PHIEDGE"] = str(float(self.parameters["PHIEDGE"]) * scaling_parameter**2)

    def scale_B(self, scaling_parameter):
        """Scales the vmec file in B"""
        self.parameters["PHIEDGE"] = str(float(self.parameters["PHIEDGE"]) * scaling_parameter)

        # self.EXTCUR *= scaling_parameter

    @staticmethod
    def rmnc_2_rmnc2D(rmnc, ntor, mpol):
        """Convert the vmec format to the two dimensional rmnc format

        Args:
            rmnc ([type]): [description]
            ntor ([type]): [description]
            mpol ([type]): [description]
        """
        rmnc_zeros = np.concatenate((np.zeros(ntor), rmnc))
        return np.reshape(rmnc_zeros, (mpol, 2 * ntor + 1))  # reshape to (mpol, 2*ntor+1)

    @staticmethod
    def rmnc2D_2_rmnc(rmnc2D, ntor, mpol):
        """Convert the the two dimensional rmnc format to the vmec format

        Args:
            rmnc ([type]): Has dimensions 2 ntor + 1, mpol !!!
            ntor ([type]): [description]
            mpol ([type]): [description]
        """
        return np.reshape(np.transpose(rmnc2D), (2 * ntor + 1) * mpol)[ntor:]

    def set_rmnc_zmns(self, rmnc, zmns, xm, xn, ntor, mpol, format="vmec"):
        """Sets the rmnc internally by vmec input format

        Args:
            format (str, optional): [description]. Defaults to "vmec". Other choice "2d"
        """
        if format == "vmec":
            self.rmnc = rmnc
            self.zmns = zmns
        else:
            self.rmnc = self.rmnc_2_rmnc2D(rmnc, ntor, mpol)
            self.zmns = self.rmnc_2_rmnc2D(zmns, ntor, mpol)

        self.xn = xn
        self.xm = xm

        return 1
