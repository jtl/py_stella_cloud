import numpy as np
from scipy.io import netcdf_file


def numpy_2_python_converter(obj):
    """
    Converts numpy to python things
    """
    if isinstance(obj, np.integer):
        return int(obj)
    elif isinstance(obj, float):
        return obj
    elif isinstance(obj, np.ndarray):
        return obj.tolist()
    else:
        return obj


def xm_xn_from_ntor_mpol(ntor, mpol):
    """
    returns xm and xn from ntor and mpol in the vmec format (1D, omitting the first ntor entries)
    """
    ntor_list = np.arange(-ntor, ntor + 1)  # +1 bc arange does not include enpoint
    mpol_list = np.arange(0, mpol)  # here we leave the +1 out as we go from 0 - mpol-1

    xm, xn = np.meshgrid(ntor_list, mpol_list)

    return xm.flatten()[ntor:], xn.flatten()[ntor:]


def vmec_nc_2_json(filename: str):
    """Generates a json object from a vmec netcdf file

    Args:
        filename (str): Path to the vmec file

    Returns:
        free_boundary_bool (bool): 1 if free, 0 if fixed
        var_dict (dict): dictionary with keys using vmec notation except phiedge and rmnc and zmns being the outermost surface
    """
    f = netcdf_file(filename, "r")

    # Check if free or fixed boundary
    free_boundary_bool = f.variables["lfreeb__logical__"][()]

    # TODO We should check if the vmec run is converged!

    # Extract all relevant parameters
    variables = {
        "nfp",
        "mpol",
        "ntor",
        "b0",
        "betatotal",
        "Aminor_p",
        "Rmajor_p",
        "volume_p",
        "IonLarmor",
        "betaxis",
        "iotaf",
        "presf",
        "raxis_cc",
        "zaxis_cs",
        "ns",
        "xn_nyq",
        "xm_nyq",
        "xn",
        "xm"
    }
    var_dict = {}
    for var in variables:
        var_dict[var] = numpy_2_python_converter(f.variables[var][()])

    var_dict["phiedge"] = numpy_2_python_converter(f.variables["phi"][()][-1])  # This is phiedge
    var_dict["rmnc"] = numpy_2_python_converter(f.variables["rmnc"][()][-1])  # LCFS
    var_dict["zmns"] = numpy_2_python_converter(f.variables["zmns"][()][-1])
    var_dict["bmnc_-1"] = numpy_2_python_converter(f.variables["bmnc"][()][-1])
    var_dict["bmnc_05"] = numpy_2_python_converter(f.variables["bmnc"][()][int(var_dict["ns"]/2)])
    var_dict["Aspect"] = numpy_2_python_converter(var_dict["Rmajor_p"] / var_dict["Aminor_p"])

    return free_boundary_bool, var_dict
