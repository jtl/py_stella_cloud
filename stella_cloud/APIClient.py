import json
import os
from pathlib import Path
from typing import Any

import numpy as np
import requests

from stella_cloud.print_statements import print_error, print_info, print_warning


class Stella_Client:
    """A client class to access data from https://stellarators.cloud"""

    def __init__(self, credential_path=None) -> None:
        # Read in email and password# Check if its in the hdf5 folder already
        script_dir = Path(os.path.dirname(__file__))  # local path
        config_filename = os.path.join(script_dir.parent, "config.json")

        if credential_path is None:
            self.credentials = json.load(open(config_filename))
        else:
            self.credentials = json.load(open(credential_path))

        self.website = "https://stellarators.cloud"

        if self.credentials["email"] is None or self.credentials["password"] is None:
            self.access_token = ""
        else:
            try:
                self.access_token_data = requests.post(
                    self.website + "/auth/login", json=self.credentials
                ).json()["data"]
                # The token works for 15 min
                self.access_token = self.access_token_data["access_token"]
                self.refresh_token = self.access_token_data["refresh_token"]
            except:
                print_error(
                    "Authentication didn't work! Check username and password in config.json!"
                    " Continuing without access token..."
                )
                self.access_token = ""

    def post(self, path: str, json: Any = None) -> None:
        """Replaces the request post by adding self.website and token and returns a json response

        Args:
            path (str): REST API path. Like "/items/collection", "/auth/login"
            body (str, optional): _description_. Defaults to None.
        """
        response = requests.post(
            self.website + path + "?access_token=" + self.access_token, json=json
        ).json()
        if "errors" in response.keys():
            print_error(response["errors"][0]["message"])
            return
        elif "data" in response.keys():
            print_info("Success! Uploaded it to collection ", path)
            return response
        else:
            print_error("Something weird happened... Sorry, I don't know what's going on.")
            return

    def get(self, path: str, json: Any = None, data: Any = None) -> None:
        """Replaces the request get by adding self.website and token and returns a json response

        Args:
            path (str): REST API path. Like "/items/collection", "/auth/login"
            body (str, optional): _description_. Defaults to None.
        """
        try:
            return requests.get(
                self.website + path + "?access_token=" + self.access_token, json=json, data=data
            ).json()["data"]
        except:
            print_error("Can't access the path ", path)
            return None

    def patch(self, path: str, id: str, name: str, newvalue: Any) -> None:
        """Replaces the request get by adding self.website and token and returns a json response

        Args:
            path (str): REST API path. Like "/items/collection", "/auth/login"
            name (str): Name of the item (e.g. rmajor, name)
            newvalue (Any): The new value, can be string, int, float or json
        """
        json = {name: newvalue}

        response = requests.patch(
            self.website + path + id + "?access_token=" + self.access_token, json=json
        ).json()
        if "errors" in response.keys():
            print_error(response["errors"][0]["message"])
            return
        elif "data" in response.keys():
            print_info("Success! Update the collection ", path)
            return response
        else:
            print_error("Something weird happened... Sorry, I don't know what's going on.")
            return

    def search(self, path: str, query: Any = None, ids_only=False) -> None:
        """Replaces the request get by adding self.website and token and returns a json response

        Args:
            path (str): REST API path. Like "/items/collection", "/auth/login"
            query (json): The search query. See https://docs.directus.io/reference/introduction/#search-http-method. Defaults to None.
            ids_only (bool): weather to return the ids only or the full data
        """
        try:
            response = requests.request(
                "SEARCH", self.website + path + "?access_token=" + self.access_token, json=query
            ).json()["data"]
            if ids_only:
                return [data["id"] for data in response]
            else:
                return response
        except:
            print_error("I don't find the requested Configuration! Misspelled? In: ", path)
        return None

    def delete(self, id: str, set="Fixed_Boundaries") -> None:
        """Tries to delete a given path

        Args:
            path (str): REST API path. Like "/items/collection", "/auth/login"
        """
        path = "/items/"+set+"/" + id

        resp = requests.request("DELETE", self.website + path + "?access_token=" + self.access_token)

        if (
            
           resp.status_code == 204
        ):
            print_info(
                "Success! Deleted it from collection ",
                path,
                " (either this or this ID wasn't in the collection in the first place)",
            )
        elif resp.status_code == 403:
            print_error("You don't have the required rights to delete this!")
        else:
            print_error(
                "That didn't work.. For some reason"
            )

        return None

    def delete_multiple(self,ids:list,set="Fixed_boundaries")->None:
        """
        
        """
        path = "/items/"+set

        body =  {
                "keys": list(ids),
                }


        resp = requests.request("DELETE", self.website + path + "?access_token=" + self.access_token, json=body)

        if (
            
           resp.status_code == 204
        ):
            print_info(
                "Success! Deleted it from collection ",
                path,
                " (either this or this ID wasn't in the collection in the first place)",
            )
        elif resp.status_code == 403:
            print_error("You don't have the required rights to delete this!")
        else:
            print_error(
                "That didn't work.. For some reason"
            )

        return None



    def get_all_fixed_boundaries(self):
        """Returns all entries in the fixed boundaries collection"""
        path = "/items/Fixed_Boundaries"
        return self.get(path)

    def get_fourier_fixed_boundaries(self):
        """Returns all entries in the fixed boundaries collection as a dictionary with keys 'id'"""
        data_dict = {}

        for data in self.get("/items/Fixed_Boundaries"):
            data_dict[data["id"]] = np.array(
                [data["Fourier_Coefficients"]["rmnc"], data["Fourier_Coefficients"]["zmns"]]
            )
        return data_dict

    def get_by_id(self, id: str, set="fixed"):
        """Returns an item from its id"""

        if set == "fixed":
            path = "/items/Fixed_Boundaries/"
        elif set == "free":
            path = "/items/Free_Boundaries/"
        elif set == "NearAxisQI":
            path = "/items/NearAxisQI/"
        else:
            print_error("No such set is present:", set)

        return self.get(path + id)

    def get_by_name(self, name: str, set="fixed"):
        """Returns an item from its id"""

        if set == "fixed":
            path = "/items/Fixed_Boundaries/"
        elif set == "free":
            path = "/items/Free_Boundaries/"
        elif set == "NearAxisQI":
            path = "/items/NearAxisQI/"
        else:
            print_error("No such set is present:", set)

        query = {"query": {"filter": {"name": {"_eq": name}}}}

        return self.search(path, query=query)

    def get_all_ids_nearAxisQI(self):
        """Returns ids of all entries in near axis QI collection"""
        return [data["id"] for data in self.get("/items/NearAxisQI")]

    def get_all_ids_fixed_boundaries(self):
        """Returns ids of all entries in fixed boundaries collection"""
        return [data["id"] for data in self.get("/items/Fixed_Boundaries")]

    def get_all_names_fixed_boundaries(self):
        """Returns names of all entries in fixed boundaries collection"""
        return [data["name"] for data in self.get("/items/Fixed_Boundaries")]

    def get_all_free_boundaries(self):
        """Returns all entries in the free boundaries collection"""
        path = "/items/Free_Boundaries"
        return self.get(path)

    def get_all_ids(self,path):
        """Returns ids of a given path"""
        return [data["id"] for data in self.get(path)]

    def add_new_field(self, name, type, default_value=None, collection="Fixed"):
        """
        Adds a new field to a given collection.

        Args:
            - name(str): Name of the field
            - type(str): Only "integer" or "float"
            - default_value(Any): Default value
            - collection(str): "Fixed" or "Free"
        """
        if type == "integer" or type == "float":
            return

        if collection == "Fixed":
            path = "/fields/Fixed_Boundaries"
        elif collection == "Free":
            path = "/fields/Free_Boundaries"
        else:
            print_error("No such collection, ", collection)

        jsonfield = {"field": name, "type": type, "schema": {"default_value": default_value}}

        return self.post(path, json=jsonfield).json()

    def post_new_NearAxisQI(self, name, beta_opt=None, **kwargs):
        """Posts a new fixed boundary to the server

        Args:
            name
            beta_opt
            kwargs consisting of the following keys:
                rmnc
                zmns
                RBC,RBS,ZBS,ZBC,nfp,ntor,mpol
                or any other that is present in the cloud dataset
        """
        from stella_cloud.utilities import numpy_2_python_converter

        # TODO: We should make sure that kwargs contains all the necessary arguments

        path = "/items/NearAxisQI"
        json_data = {
            "name": name,
            "Fourier_Coefficients": {
                "rmnc": numpy_2_python_converter(kwargs.pop("rmnc", None)),
                "rmns": numpy_2_python_converter(kwargs.pop("rmns", None)),
                "zmns": numpy_2_python_converter(kwargs.pop("zmns", None)),
                "zmnc": numpy_2_python_converter(kwargs.pop("zmnc", None)),
                "B0_vals": numpy_2_python_converter(kwargs.pop("B0_vals", None)),
            },
            "Axis": {
                "raxis_cc": numpy_2_python_converter(kwargs.pop("RBC", None)),
                "raxis_cs": numpy_2_python_converter(kwargs.pop("RBS", None)),
                "zaxis_cs": numpy_2_python_converter(kwargs.pop("ZBS", None)),
                "zaxis_cc": numpy_2_python_converter(kwargs.pop("ZBC", None)),
            },
            "nfp": kwargs.pop("nfp", None),
            "beta_opt": beta_opt,
            "mpol": kwargs.pop("mpol", None),
            "ntor": kwargs.pop("ntor", None),
        }

        for key in kwargs.keys():
            json_data[key] = kwargs.get(key)

        # for key in kwargs.keys():
        #    print_warning("In 'post_new_NearAxisQI'. Unused Argument: ", key)

        return self.post(path, json=json_data)

    def post_new_NearAxisQH(self, name, beta_opt=None, **kwargs):
        """Posts a new fixed boundary to the server

        Args:
            name
            beta_opt
            kwargs consisting of the following keys:
                rmnc
                zmns
                RBC,RBS,ZBS,ZBC,nfp,ntor,mpol
        """
        from stella_cloud.utilities import numpy_2_python_converter

        # TODO: We should make sure that kwargs contains all the necessary arguments

        path = "/items/Near_Axis_QH"
        json_data = {
            "name": name,
            "Fourier_Coefficients": {
                "rmnc": numpy_2_python_converter(kwargs.pop("rmnc", None)),
                "rmns": numpy_2_python_converter(kwargs.pop("rmns", None)),
                "zmns": numpy_2_python_converter(kwargs.pop("zmns", None)),
                "zmnc": numpy_2_python_converter(kwargs.pop("zmnc", None)),
            },
            "Axis": {
                "raxis_cc": numpy_2_python_converter(kwargs.pop("RBC", None)),
                "raxis_cs": numpy_2_python_converter(kwargs.pop("RBS", None)),
                "zaxis_cs": numpy_2_python_converter(kwargs.pop("ZBS", None)),
                "zaxis_cc": numpy_2_python_converter(kwargs.pop("ZBC", None)),
            },
            "nfp": kwargs.pop("nfp", None),
            "beta_opt": beta_opt,
            "mpol": kwargs.pop("mpol", None),
            "ntor": kwargs.pop("ntor", None),
        }
        for key in kwargs.keys():
            json_data[key] = kwargs.get(key)

        return self.post(path, json=json_data)

    def post_new_fixed_boundary(self, name, symmetry=None, beta_opt=None, **kwargs):
        """Posts a new fixed boundary to the server

        Args:
            name
            beta_opt
            kwargs
        """
        from stella_cloud.utilities import numpy_2_python_converter

        # TODO: We should make sure that kwargs contains all the necessary arguments

        path = "/items/Fixed_Boundaries"
        json_data = {
            "name": name,
            "Fourier_Coefficients": {
                "rmnc": numpy_2_python_converter(kwargs.pop("rmnc", None)),
                "zmns": numpy_2_python_converter(kwargs.pop("zmns", None)),
                "bmnc_-1": numpy_2_python_converter(kwargs.pop("bmnc_-1", None)),
                "bmnc_05": numpy_2_python_converter(kwargs.pop("bmnc_05", None)),
                "xm": numpy_2_python_converter(kwargs.pop("xm", None)),
                "xn": numpy_2_python_converter(kwargs.pop("xn", None)),
                "xm_nyq": numpy_2_python_converter(kwargs.pop("xm_nyq", None)),
                "xn_nyq": numpy_2_python_converter(kwargs.pop("xn_nyq", None))
            },
            "Axis": {
                "raxis_cc": numpy_2_python_converter(kwargs.pop("raxis_cc", None)),
                "zaxis_cs": numpy_2_python_converter(kwargs.pop("zaxis_cs", None)),
            },
            "nfp": kwargs.pop("nfp", None),
            "ns": kwargs.pop("ns", None),
            "beta_opt": beta_opt,
            "phiedge": kwargs.pop("phiedge", None),
            "mpol": kwargs.pop("mpol", None),
            "ntor": kwargs.pop("ntor", None),
            "b0": kwargs.pop("b0", None),
            "betatotal": kwargs.pop("betatotal", None),
            "Aminor_p": kwargs.pop("Aminor_p", None),
            "Rmajor_p": kwargs.pop("Rmajor_p", None),
            "Aspect": kwargs.pop("Aspect", None),
            "volume_p": kwargs.pop("volume_p", None),
            "IonLarmor": kwargs.pop("IonLarmor", None),
            "betaxis": kwargs.pop("betaxis", None),
            "Profiles": {
                "iotaf": numpy_2_python_converter(kwargs.pop("iotaf", None)),
                "presf": numpy_2_python_converter(kwargs.pop("presf", None)),
            },
            "symmetry": [symmetry],
        }
        for key in kwargs.keys():
            print_warning("In 'post_new_fixed_boundary'. Unused Argument: ", key)

        return self.post(path, json=json_data)

    def post_new_free_boundary(self, name, symmetry=None, beta_opt=None, **kwargs):
        """Posts a new free boundary to the server
        For now just the same as fixed but in the future should have a relation to a coils file

        Args:
            name
            beta_opt
            symmetry (str): Either "QI", "QA", "QH" or "TOK"
            kwargs
        """
        from stella_cloud.utilities import numpy_2_python_converter

        if symmetry not in ["QI", "QA", "QH", "TOK"]:
            print_warning(
                "Your symmetry is invalid. Give me either QI, QA, QH or TOK. Setting it to None"
            )
            symmetry = None

        # TODO: We should make sure that kwargs contains all the /necessary/ arguments

        path = "/items/Free_Boundaries"
        json_data = {
            "name": name,
            "Fourier_Coefficients": {
                "rmnc": numpy_2_python_converter(kwargs.pop("rmnc", None)),
                "zmns": numpy_2_python_converter(kwargs.pop("zmns", None)),
            },
            "Axis": {
                "raxis_cc": numpy_2_python_converter(kwargs.pop("raxis_cc", None)),
                "zaxis_cs": numpy_2_python_converter(kwargs.pop("zaxis_cs", None)),
            },
            "nfp": kwargs.pop("nfp", None),
            "beta_opt": beta_opt,
            "phiedge": kwargs.pop("phiedge", None),
            "mpol": kwargs.pop("mpol", None),
            "ntor": kwargs.pop("ntor", None),
            "b0": kwargs.pop("b0", None),
            "betatotal": kwargs.pop("betatotal", None),
            "Aminor_p": kwargs.pop("Aminor_p", None),
            "Rmajor_p": kwargs.pop("Rmajor_p", None),
            "Aspect": kwargs.pop("Aspect", None),
            "volume_p": kwargs.pop("volume_p", None),
            "IonLarmor": kwargs.pop("IonLarmor", None),
            "betaxis": kwargs.pop("betaxis", None),
            "Profiles": {
                "iotaf": numpy_2_python_converter(kwargs.pop("iotaf", None)),
                "presf": numpy_2_python_converter(kwargs.pop("presf", None)),
            },
            "symmetry": [symmetry],
        }
        for key in kwargs.keys():
            print_warning("In 'post_new_free_boundary'. Unused Argument: ", key)

        return self.post(path, json=json_data)

    def post_fixed_boundary_from_vmecobject(self, wout: Any, name, symmetry=None, beta_opt=None):
        """Posts a new fixed boundary to the server

        Args:
            vmec: Needs to be a vmec.wout object as obtained from the python wrapped VMEC version
            name
            beta_opt
            kwargs
        """
        from stella_cloud.utilities import numpy_2_python_converter

        # TODO: We should make sure that kwargs contains all the necessary arguments

        path = "/items/Fixed_Boundaries"
        json_data = {
            "name": name,
            "Fourier_Coefficients": {
                "rmnc": numpy_2_python_converter(wout.rmnc[:, -1]),
                "zmns": numpy_2_python_converter(wout.zmns[:, -1]),
            },
            "Axis": {
                "raxis_cc": numpy_2_python_converter(wout.raxis_cc),
                "zaxis_cs": numpy_2_python_converter(wout.zaxis_cs),
            },
            "nfp": int(wout.nfp),
            "beta_opt": beta_opt,
            "phiedge": wout.phi[-1],
            "mpol": int(wout.mpol),
            "ntor": int(wout.ntor),
            "b0": wout.b0,
            "betatotal": wout.betatotal,
            "Aminor_p": wout.Aminor_p,
            "Rmajor_p": wout.Rmajor_p,
            "Aspect": wout.Rmajor_p / wout.Aminor_p,
            "volume_p": wout.volume_p,
            "IonLarmor": wout.IonLarmor,
            "betaxis": wout.betaxis,
            "Profiles": {
                "iotaf": numpy_2_python_converter(wout.iotaf),
                "presf": numpy_2_python_converter(wout.presf),
            },
            "symmetry": [symmetry],
        }

        return self.post(path, json=json_data)

    def post_from_vmec_nc(self, filename: str, name: str, symmetry: str = None, beta_opt=None):
        """Posts a boundary from a vmec.nc file to the server

        Args:
            filename (str): Filename of the fixed or free boundary .nc file
            name (str): Name of the configuration (Human Readable)
            beta_opt (float): Volume beta it was optimized at
        """
        from stella_cloud.utilities import vmec_nc_2_json

        # Obtain the variables and the boolean if it is fixed or free
        free_bool, var_dict = vmec_nc_2_json(filename)

        if free_bool:
            # Free boundary
            return self.post_new_free_boundary(
                name=name, symmetry=symmetry, beta_opt=beta_opt, **var_dict
            )
        else:
            # Fixed boundary
            return self.post_new_fixed_boundary(
                name=name, symmetry=symmetry, beta_opt=beta_opt, **var_dict
            )
