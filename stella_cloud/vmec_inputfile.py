"""
vmec_inputfile.py
====================================
A module that parses and writes vmec input files

.. moduleauthor:: Jorrit Lion <jorrit.lion@ipp.mpg.de>
"""
import numpy as np
from presprocess.print_format import print_error, print_warning


class vmec_input_file:
    """
    represents the vmec input file
    """

    def __init__(self, **kwargs):
        """Initializes the vmec file from a given file

        Args:
            filename ([str]): Location of the vmec (free or fixed boundary)
        """
        self.rmnc = kwargs.pop("rmnc", None)
        self.zmns = kwargs.pop("zmns", None)
        self.xn = kwargs.pop("xn", None)
        self.xm = kwargs.pop("xm", None)
        self.ntor = kwargs.pop("ntor", None)
        self.mpol = kwargs.pop("mpol", None)
        self.raxis = kwargs.pop("raxis", None)
        self.zaxis = kwargs.pop("zaxis", None)
        self.parameters = kwargs.pop("parameters", None)

        self.NFP = kwargs.pop("NFP", None)
        self.EXTCUR = kwargs.pop("EXTCUR", None)
        self.PHIEDGE = kwargs.pop("PHIEDGE", None)

    @classmethod
    def parse(cls, filelocation: str):
        """
        Loads the class from a vmec input file
        """
        rmnc, _, _ = cls.parse_rmnc(filelocation)
        zmns, xn, xm = cls.parse_zmns(filelocation)

        raxis = cls.parse_r_axis(filelocation)
        zaxis = cls.parse_z_axis(filelocation)

        parameters = cls.parse_parameters(filelocation)

        extcur = cls.parse_extcur(filelocation)
        phiedge = cls.parse_phiedge(filelocation)

        if parameters["NFP"] is not None:
            try:
                nfp = int(parameters["NFP"])
            except BaseException:
                # Either parameter has a comma at the end or is not present at all
                try:
                    nfp = int(
                        parameters["NFP"][:-1]
                    )  # Getting rid of the comma that might be there at the end
                except BaseException:
                    print_error(
                        "Something is weird. Check me. Could be that NFP has this form in the file"
                        ' "NFP = 2 ,".                                  In this case remove the'
                        " space between the 2 and the comma. Initializing with symmetry 1"
                    )
                    nfp = 1
        else:
            print_warning("No NFP found. Initializing with symmetry 1")
            nfp = 1

        mpol = max(xm) + 1
        ntor = int(max(xn) / nfp)  # not super sure about these two
        xn = xn * nfp  # to match the vmec convention

        return cls(
            rmnc=rmnc,
            zmns=zmns,
            raxis=raxis,
            zaxis=zaxis,
            parameters=parameters,
            NFP=nfp,
            EXTCUR=extcur,
            PHIEDGE=phiedge,
            xn=xn,
            xm=xm,
            ntor=ntor,
            mpol=mpol,
        )

    @classmethod
    def from_netcdf_and_input(cls, inputfile: str, vmecnetcdf_path: str):
        """
        Loads the class from a vmec input file BUT uses the netcdf file for the boundary

        CARE: xn are NOT the vmec xns they are vmec xn/nfp
        """
        import netCDF4

        try:
            wout_netcdf = netCDF4.Dataset(vmecnetcdf_path)
        except:
            print_error("No such vmec netcdf path:", vmecnetcdf_path)
            return

        zmns = wout_netcdf.variables["zmns"][:][-1]
        rmnc = wout_netcdf.variables["rmnc"][:][-1]

        nfp = int(wout_netcdf.variables["nfp"][:])

        xn = np.array(wout_netcdf.variables["xn"][:] / nfp).astype(int)
        xm = np.array(wout_netcdf.variables["xm"][:]).astype(int)

        ntor = wout_netcdf.variables["ntor"][:]
        mpol = wout_netcdf.variables["mpol"][:]

        raxis = wout_netcdf.variables["raxis_cc"][:]
        zaxis = wout_netcdf.variables["zaxis_cs"][:]

        parameters = cls.parse_parameters(inputfile)

        extcur = cls.parse_extcur(inputfile)
        phiedge = cls.parse_phiedge(inputfile)

        return cls(
            rmnc=rmnc,
            zmns=zmns,
            raxis=raxis,
            zaxis=zaxis,
            parameters=parameters,
            NFP=nfp,
            EXTCUR=extcur,
            PHIEDGE=phiedge,
            xn=xn,
            xm=xm,
            ntor=ntor,
            mpol=mpol,
        )

    @staticmethod
    def parse_rmnc(filelocation: str):
        """
        Parses the vmec file and returns it as an (2*ntor+1,mpol) array
        """
        import re

        # Read in RBC
        raw_content = [
            re.findall(
                r"(?:RBC|rbc)\([\s]*([\-0-9]*)[\s]*,[\s]*([0-9]*)\)[\s]*=[\s]*([-+0-9\.eE]*)",
                open(filelocation).read(),
            )
        ]
        raw_content = list(filter(None, raw_content))[0]

        nlist = []
        mlist = []
        rmnc = []

        for item in raw_content:
            n = int(item[0])  # item is of format [('0', '0', '5.5586e+00')]
            m = int(item[1])
            number = float(item[2])
            nlist.append(n)
            mlist.append(m)
            rmnc.append(number)

        return rmnc, nlist, mlist

    @staticmethod
    def parse_zmns(filelocation: str):
        """
        Parses the vmec file and returns it as an (2*ntor+1,mpol) array
        """
        import re

        # Read in zmns
        raw_content = [
            re.findall(
                r"(?:ZBS|zbs)\([\s]*([\-0-9]*)[\s]*,[\s]*([0-9]*)\)[\s]*=[\s]*([-+0-9\.eE]*)",
                open(filelocation).read(),
            )
        ]
        raw_content = list(filter(None, raw_content))[0]

        nlist = []
        mlist = []
        zmns = []

        for item in raw_content:
            n = int(item[0])  # item is of format [('0', '0', '5.5586e+00')]
            m = int(item[1])
            number = float(item[2])
            nlist.append(n)
            mlist.append(m)
            zmns.append(number)

        return zmns, nlist, mlist

    @staticmethod
    def parse_r_axis(filelocation: str):
        """
        Parses the vmec file and returns it as an (2*ntor+1,mpol) array
        """
        import re

        import numpy as np

        # Read in zmns

        raw_content = [
            re.findall(r"(?:RAXIS_CC|RAXIS)[\s]*=[\s]*([-+0-9\.eE\s]*)$", line)
            for line in open(
                filelocation
            )  # missing space at the end is important (compared to rbc)
        ]
        raw_content = list(filter(None, raw_content))[-1]  # take the last occurence
        raxis = np.array(raw_content[0].split()).astype(np.float64)

        return raxis

    @staticmethod
    def parse_z_axis(filelocation: str):
        """
        Parses the vmec file and returns it as an (2*ntor+1,mpol) array
        """
        import re

        import numpy as np

        # Read in zmns

        raw_content = [
            re.findall(r"(?:ZAXIS_CS|ZAXIS)[\s]*=[\s]*([-+0-9\.eE\s]*)$", line)
            for line in open(
                filelocation
            )  # missing space at the end is important (compared to rbc)
        ]
        raw_content = list(filter(None, raw_content))[-1]  # take the last occurence
        zaxis = np.array(raw_content[0].split()).astype(np.float64)

        return zaxis

    @staticmethod
    def parse_extcur(filelocation: str):
        """
        Parses the vmec file and returns it as an (2*ntor+1,mpol) array
        """
        import re

        import numpy as np

        # Read in zmns

        try:

            raw_content = [
                re.findall(r"(?:EXTCUR)[\s]*=[\s]*([-+0-9\.eE]*)", open(filelocation).read())
            ]
            raw_content = list(filter(None, raw_content))[-1]  # take the last occurence

            try:
                EXTCUR = np.array(raw_content[0].split(",")).astype(np.float64)
            except:  # tried to split on komma. If this does not work try to split on space
                EXTCUR = np.array(raw_content[0].split()).astype(np.float64)

        except:
            print_warning("No EXTCUR found. Fixed Boundary? No problemo, setting it to None.")
            EXTCUR = None  # [0, 0, 0, 0, 0]

        return EXTCUR

    @staticmethod
    def parse_phiedge(filelocation: str):
        """
        Parses the vmec file and returns it as an (2*ntor+1,mpol) array
        """
        import re

        import numpy as np

        # Read in zmns

        try:

            raw_content = [
                re.findall(r"(?:PHIEDGE)[\s]*=[\s]*([-+0-9\.eE]*)", open(filelocation).read())
            ]
            raw_content = list(filter(None, raw_content))[-1]  # take the last occurence

            PHIEDGE = np.array(raw_content[0].split()).astype(np.float64)

        except:
            print_warning(
                "No PHIEDGE found. Fixed Boundary? No problemo, setting it to some small value."
            )
            PHIEDGE = 0.1  # [0, 0, 0, 0, 0]

        return PHIEDGE

    @staticmethod
    def parse_parameters(filelocation: str):
        """
        Parses the vmec file and returns all set parameters as a dictionary
        """
        import re

        raw_content = [
            re.findall(r"([\w\d]+) *= *([-+0-9\.eE, TF]+)[\n\s]", open(filelocation).read())
        ]
        raw_content = list(filter(None, raw_content))[0]

        mydict = {}
        for item in raw_content:
            mydict[str(item[0])] = item[1]

        # Now add the things with a string
        # Like:  MGRID_FILE = 'mgrid_w7x_nv36_hires.nc'
        raw_content = [
            re.findall(r"([\w\d]+) *= *([\'\"][\w_\.]+[\'\"])[\n\s]*", open(filelocation).read())
        ]
        raw_content = list(filter(None, raw_content))[0]

        for item in raw_content:
            mydict[str(item[0])] = item[1]

        # Delete Raxis and Zaxis
        if "RAXIS" in mydict.keys():
            mydict.pop("RAXIS")
        if "ZAXIS" in mydict.keys():
            mydict.pop("ZAXIS")
        if "ZAXIS_CS" in mydict.keys():
            mydict.pop("ZAXIS_CS")
        if "RAXIS_CC" in mydict.keys():
            mydict.pop("RAXIS_CC")

        return mydict

    def write_input_file(self, filelocation):
        """
        Args:
            filelocation ([type]): [description]
        """

        # First write parameters
        with open(filelocation, "w") as out:
            out.write("&INDATA\n")

            # First write parameters
            for key in self.parameters.keys():
                if key != "EXTCUR" and key != "PHIEDGE":
                    out.write("{:} = {:}\n".format(key, self.parameters[key]))

            # Write external current array
            if self.EXTCUR is not None:
                out.write("EXTCUR = {:}\n".format(", ".join(self.EXTCUR.astype(np.str_))))

            # Write Phiedge
            if self.EXTCUR is not None:
                out.write("PHIEDGE = {:}\n".format(", ".join(self.PHIEDGE.astype(np.str_))))

            # Axis
            out.write("RAXIS = {:}\n".format(" ".join(np.array(self.raxis).astype(np.str_))))
            out.write("ZAXIS = {:}\n".format(" ".join(np.array(self.zaxis).astype(np.str_))))

            # two_ntor_p1, mpol_p1 = np.shape(self.zmns)

            # ntor = int((two_ntor_p1-1)/2)

            for xm, xn, rmnc, zmns in zip(self.xm, self.xn, self.rmnc, self.zmns):
                out.write("RBC({:},{:}) = {:}  ".format(xn, xm, rmnc))
                out.write("ZBS({:},{:}) = {:}\n".format(xn, xm, zmns))

            out.write("/\n")

    def scale_R(self, scaling_parameter):
        """Scales the vmec file in R"""
        self.rmnc = np.multiply(
            self.rmnc, scaling_parameter
        )  # If not np.multiply, it throws an "can't multiply sequence by non-int of type 'numpy.float64'" error...
        self.zmns = np.multiply(self.zmns, scaling_parameter)
        self.raxis = np.multiply(self.raxis, scaling_parameter)
        self.zaxis = np.multiply(self.zaxis, scaling_parameter)

        self.PHIEDGE *= scaling_parameter**2

    def scale_B(self, scaling_parameter):
        """Scales the vmec file in B"""
        self.PHIEDGE *= scaling_parameter

        # self.EXTCUR *= scaling_parameter

    def return_rmnc_2D(self):
        """Returns the rmnc VMEC file as a two 2D array with dimensions 2ntor+1,mpol

        Returns:
            rmnc2D: The Rmncs in 2D
        """

        return self.rmnc2D_2_rmnc(self.rmnc, self.ntor, self.mpol)

    def return_zmns_2D(self):
        """Returns the rmnc VMEC file as a two 2D array with dimensions 2ntor+1,mpol

        Returns:
            zmns2D: The zmns in 2D
        """

        return self.rmnc2D_2_rmnc(self.zmns, self.ntor, self.mpol)

    @staticmethod
    def rmnc_2_rmnc2D(rmnc, ntor, mpol):
        """Convert the vmec format to the two dimensional rmnc format

        Args:
            rmnc ([type]): [description]
            ntor ([type]): [description]
            mpol ([type]): [description]
        """
        rmnc_zeros = np.concatenate((np.zeros(ntor), rmnc))
        return np.reshape(rmnc_zeros, (mpol, 2 * ntor + 1))  # reshape to (mpol, 2*ntor+1)

    @staticmethod
    def rmnc2D_2_rmnc(rmnc2D, ntor, mpol):
        """Convert the the two dimensional rmnc format to the vmec format

        Args:
            rmnc ([type]): Has dimensions 2 ntor + 1, mpol !!!
            ntor ([type]): [description]
            mpol ([type]): [description]
        """
        return np.reshape(np.transpose(rmnc2D), (2 * ntor + 1) * mpol)[ntor:]

    def set_rmnc_zmns(self, rmnc, zmns, xm, xn, ntor, mpol, format="vmec"):
        """Sets the rmnc internally by vmec input format

        Args:
            format (str, optional): [description]. Defaults to "vmec". Other choice "2d"
        """
        if format == "vmec":
            self.rmnc = rmnc
            self.zmns = zmns
        else:
            self.rmnc = self.rmnc_2_rmnc2D(rmnc, ntor, mpol)
            self.zmns = self.rmnc_2_rmnc2D(zmns, ntor, mpol)

        self.xn = xn
        self.xm = xm

        return 1

    def get_rmajor_rminor(self):
        """Returns major and minor radius (just an estimation for now)"""
        return self.rmnc[0], np.sqrt(
            self.rmnc[2 * self.ntor + 1] ** 2 + self.zmns[2 * self.ntor + 1] ** 2
        )
