# Demonstrates how to run VMEC from a database entry


from simsopt.mhd import Vmec
from simsopt.util import MpiPartition, log

from stella_cloud.APIClient import Stella_Client
from stella_cloud.vmec_input_file import vmec_input_file

client = Stella_Client()

things = client.get_all_ids_nearAxisQI()
# things = client.get_all_ids_fixed_boundaries()

id = things[-2]

conf = client.get_by_id(things[-1], set="NearAxisQI")

st = vmec_input_file.from_stella_cloud_id(id, set="NearAxisQI")

st.write_input_file("input.NearAxisQI")

log()
# In the next line, we can adjust how many groups the pool of MPI
# processes is split into.
mpi = MpiPartition(ngroups=3)
mpi.write()

equil = Vmec("input.NearAxisQI", mpi)

# Reduce Fourier Resolution
equil.indata.ntor = 6
equil.indata.mpol = 6

equil.run()

# Increase Fourier Resolution
equil.indata.ntor = 12
equil.indata.mpol = 12

equil.run()

client = Stella_Client()
client.post_fixed_boundary_from_vmecobject(equil.wout, conf["name"], "QI", 0.0)


# surf = equil.boundary
