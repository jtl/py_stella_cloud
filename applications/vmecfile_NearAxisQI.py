"""
Demonstrates how to obtain an input file from a given hash
"""

from stella_cloud.APIClient import Stella_Client
from stella_cloud.vmec_input_file import vmec_input_file

client = Stella_Client()
ids = client.get_all_ids_nearAxisQI()

st = vmec_input_file.from_stella_cloud_id(ids[0], set="NearAxisQI")

st.write_input_file("NearAxisTest.input")
