"""
Demonstrates how to do a basic search
"""

from stella_cloud.APIClient import Stella_Client

client = Stella_Client()

# Get all near axis guesses with nfp = 3
query = {"query": {"filter": {"nfp": {"_eq": 3}}}}

# Option ids_only makes it return ids only. Otherwise all the data is returned
confs = client.search("/items/NearAxisQI", query, ids_only=True)

print("Found", len(confs), " Configurations!")

# It returns the full objects:
print(confs)

query = {"query": {"filter": {"vmec_approved": {"_null": True}}}}
confs = client.search("/items/NearAxisQI", query, ids_only=True)
print("Found", len(confs), " unapproved configurations!")
