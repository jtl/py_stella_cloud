"""
Demonstrates how to add new items to a collection together with a default value.
Will only work if you have the correct access rights
"""


import numpy as np

from stella_cloud.APIClient import Stella_Client

to_be_added = {
    "CURTOR": 0.0,
    "NCURR": 1,
    "PRECON_TYPE": "power_series",
    "PMASS_TYPE": "power_series",
    "PIOTA_TYPE": "power_series",
    "PCURR_TYPE": "power_series",
    "SPRES_PED": 1.0,
    "PRES_SCALE": 1.0,
}

client = Stella_Client()
path = "/fields/Free_Boundaries"


for key in to_be_added.keys():
    obj = to_be_added[key]
    if isinstance(obj, np.integer):
        obj = int(obj)
        mytype = "integer"
    elif isinstance(obj, int):
        obj = int(obj)
        mytype = "integer"
    elif isinstance(obj, float):
        mytype = "float"
    elif isinstance(obj, str):
        mytype = "string"
    else:
        print("Skipping ", key)
        continue

    jsonfield = {"field": key, "type": mytype, "schema": {"default_value": None}}
    print(client.post(path, json=jsonfield))
