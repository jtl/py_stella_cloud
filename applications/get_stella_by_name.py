"""
Demonstrates how to obtain a stella object by its name
"""

from stella_cloud.vmec_input_file import vmec_input_file

st = vmec_input_file.from_stella_cloud_name("JIM_MATT_QH_5", set="free")


st.write_input_file("JIM.input")
