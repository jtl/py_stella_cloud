"""
Demonstrates how to obtain an input file from a given hash
"""

from stella_cloud.vmec_input_file import vmec_input_file

st = vmec_input_file.from_stella_cloud_id("844031a5-1dde-461c-aabc-2f45c1c5f593")


st.write_input_file("testfile.input")
