import numpy as np

from stella_cloud.APIClient import Stella_Client
from stella_cloud.utilities import vmec_nc_2_json

_, vmec_dict = vmec_nc_2_json("data/wout_HELIAS.nc")

client = Stella_Client()
path = "/fields/Free_Boundaries"

variables = {
    "nfp",
    "mpol",
    "ntor",
    "b0",
    "betatotal",
    "Aminor_p",
    "Rmajor_p",
    "volume_p",
    "IonLarmor",
    "betaxis",
    "phi",
    "iotaf",
    "presf",
    "raxis_cc",
    "zaxis_cs",
}

for key in vmec_dict.keys():
    obj = vmec_dict[key]
    if isinstance(obj, np.integer):
        obj = int(obj)
        mytype = "integer"
    elif isinstance(obj, int):
        obj = int(obj)
        mytype = "integer"
    elif isinstance(obj, float):
        mytype = "float"
    else:
        print("Skipping ", key)
        continue

    jsonfield = {"field": key, "type": mytype, "schema": {"default_value": None}}
    print(client.post(path, json=jsonfield).json())
