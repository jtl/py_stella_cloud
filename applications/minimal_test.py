"""
Demonstrates how to show all available hashes
"""

from stella_cloud.APIClient import Stella_Client

client = Stella_Client()

print(client.get("/items/Fixed_Boundaries"))
