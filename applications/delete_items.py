"""
Demonstrates how to do a basic search
"""

from stella_cloud.APIClient import Stella_Client

client = Stella_Client()

itemsleft = True


while itemsleft:
    # Get all near axis guesses with nfp = 1
    query = {"query": {"filter": {"nfp": {"_eq": 1}}}}

    # Option ids_only makes it return ids only. Otherwise all the data is returned
    ids = client.search("/items/NearAxisQI", query, ids_only=True)
    if len(ids)==0:
        itemsleft=False
    else:
        for id in ids:
            client.delete(id,set="NearAxisQI")
