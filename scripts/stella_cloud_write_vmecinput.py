#! python
import sys

from stella_cloud.print_statements import print_error, print_warning
from stella_cloud.vmec_input_file import vmec_input_file

print("Sys.argv:", sys.argv)

if len(sys.argv) == 2:
    print_warning(
        "System Arguments 1. I am saving the file to `input.stella_cloud` and look through fixed"
        " boundaries only!"
    )
    name = str(sys.argv[1])
    filename = "input.stella_cloud"
    set = "fixed"
elif len(sys.argv) == 3:
    print_warning("System Arguments 2. I am looking through fixed boundaries only!")
    name = str(sys.argv[1])
    filename = str(sys.argv[2])
    set = "fixed"
elif len(sys.argv) == 4:
    name = str(sys.argv[1])
    filename = str(sys.argv[2])
    set = str(sys.argv[3])
    if set != "fixed" and set != "free":
        print_error("the third argument should be 'free' or 'fixed', you gave me: ", set)
        sys.exit("Stopping program")
else:
    print_error(
        "System Arguments not 2 or 3. Use it `stella_could_write_vmecinput name input.Free` or"
        " `stella_could_write_vmecinput name`"
    )
    sys.exit("Stopping program")


st = vmec_input_file.from_stella_cloud_name(name, set=set)

st.write_input_file(filename)
