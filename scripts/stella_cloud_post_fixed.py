#! python
import sys

from stella_cloud.APIClient import Stella_Client
from stella_cloud.print_statements import print_error, print_warning

print("Sys.argv:", sys.argv)

if len(sys.argv) == 3:
    print_warning(
        "System Arguments 2. Better use 3 arguments, `stella_could_post_fixed vmec.nc name"
        " symmetry`, where symmetry `QI`, `QA`, `QH` or `Tok`.. or `none`"
    )
    filename = str(sys.argv[1])
    name = str(sys.argv[2])
    symmetry = None
elif len(sys.argv) == 4:
    filename = str(sys.argv[1])
    name = str(sys.argv[2])
    symmetry = str(sys.argv[3])
else:
    print_error(
        "System Arguments not 2 or 3. Use it `stella_could_post_fixed vmec.nc name` or"
        " `stella_could_post_fixed vmec.nc name symmetry`, where symmetry `QI`, `QA`, `QH` or `Tok`"
    )
    sys.exit("Stopping program")

client = Stella_Client()
print(client.post_from_vmec_nc(filename, name, symmetry=symmetry))
