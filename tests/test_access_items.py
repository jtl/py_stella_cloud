import unittest

from stella_cloud.APIClient import Stella_Client


class test_access_public(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(test_access_public, self).__init__(*args, **kwargs)
        self.client = Stella_Client()

    def test01_get_public(self):
        """
        Checks if we can access the public collection
        """
        length_fixed_boundaries = len(self.client.get("/items/Fixed_Boundaries"))

        self.assertGreater(
            length_fixed_boundaries,
            0,
            "I could access the public fixed boundary database and it is not empty. Good.",
        )

    def test02_access_ids(self):
        """
        Checks if all ids can be accessed
        """
        ids = self.client.get_all_ids_fixed_boundaries()
        self.assertGreater(
            len(ids),
            0,
            "I could access the ids of the fixed boundary database and it is not empty. Good.",
        )

    def test03_access_by_id(self):
        """Checks if we get an item by id"""
        ids = self.client.get_all_ids_fixed_boundaries()
        data = self.client.get_by_id(ids[1])
        self.assertGreater(
            len(data),
            0,
            "I could access an item of the fixed boundary database by its id and it is not empty."
            " Good.",
        )

    def test04_access_NearAxis(self):
        """Checks if we get an item by id"""
        ids = self.client.get_all_ids_nearAxisQI()
        data = self.client.get_by_id(ids[1],set="NearAxisQI")
        self.assertGreater(
            len(data),
            0,
            "I could access an item of the fixed boundary database by its id and it is not empty."
            " Good.",
        )


def main():
    unittest.main()


if __name__ == "__main__":
    main()
