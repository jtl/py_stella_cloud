# Python Stella Cloud

gitbook pages can be found here: https://jtl.pages.mpcdf.de/py_stella_cloud


Usage:
```bash
git clone <reponame>
```
Go the package directory, e.g. `cd <reponame>` 
```bash
pip install -e .
```


The utility should be:

##### Upload a VMEC fixed boundary .nc file

works:
```bash
stella_cloud_post_fixed.py PATH_TO_VMECFILE.nc NAME SYMMETRY
```
The script automatically distinguishes between fixed and free boundary runs and uploads it to the correct directory.
##### Upload a VMEC input file

##### Upload a VMEC free boundary .nc file

works.
TODO: should be in relation to a coils file

##### Upload a coils file in relation to a fixed boundary .nc file

##### Upload a coils file in relation to a free boundary .nc file

##### Write 

You can write a a vmec input file by providing the name of the configuration, the input file name you want to write it to and an argument for "fixed" or "free", dependent on which database you want to look for.
```bash
stella_could_write_vmecinput.py name input.Free free/fixed
```

Further more it should do more:

 - Provide the necessary data (e.g. the VMEC input file) for a given configuration name
 - provide names for a given tag, e.g. "QI" or "SIMSOPT", means it should be able to pre-select data from the database and provide the unique ID


### Entities

A complete set of information defining a stellarator configuration is

 - Fixed boundary VMEC input file
 - Coils file
 - Free boundary VMEC input file
 - makegrid input file
 - mgrid file (optional)
 - beams3d input file
 - SIMPLE (??)
 - COBRA (??)
 - 


Good to have

 - SIMSOPT input files
 - coil optimization input files
 - 